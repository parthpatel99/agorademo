import 'package:flutter/material.dart';
import 'package:soprano_test/pages/SplashPage.dart';
import 'package:soprano_test/pages/StreamPage.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Soprano',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      initialRoute: '/',
      routes: {
        '/': (context) => SplashPage(),
        '/stream': (context) => StreamPage(ModalRoute.of(context).settings.arguments)
      },
    );
  }
}