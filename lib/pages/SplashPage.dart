import 'package:agora_rtc_engine/rtc_engine.dart';
import 'package:flutter/material.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:soprano_test/utils/GradientButton.dart';
import 'package:soprano_test/utils/PermissionUtils.dart';

class SplashPage extends StatefulWidget {
  @override
  _SplashPageState createState() => _SplashPageState();
}

class _SplashPageState extends State<SplashPage> {
  ClientRole role;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        alignment: Alignment.center,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            GradientButton(
              onPressed: () {
                getPermissions().then((_) => Navigator.of(context).pushNamed(
                    '/stream',
                    arguments: {'role': ClientRole.Broadcaster}));
              },
              child: Text("Join as Broadcaster"),
            ),
            GradientButton(
              onPressed: () {
                getPermissions().then((_) => Navigator.of(context).pushNamed(
                    '/stream',
                    arguments: {'role': ClientRole.Audience}));
              },
              child: Text("Join as Audience"),
            ),
          ],
        ),
      ),
    );
  }

  Future<void> getPermissions() async {
    await PermissionUtils.handlePermission(Permission.camera);
    await PermissionUtils.handlePermission(Permission.microphone);
  }
}
