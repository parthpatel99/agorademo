import 'dart:async';

import 'package:agora_rtc_engine/rtc_engine.dart';
import 'package:agora_rtc_engine/rtc_local_view.dart' as RtcLocalView;
import 'package:agora_rtc_engine/rtc_remote_view.dart' as RtcRemoteView;
import 'package:flutter/material.dart';
import 'package:soprano_test/constants/AppConstants.dart';
import 'package:flutter_icons/flutter_icons.dart';

class StreamPage extends StatefulWidget {
  /// non-modifiable client role of the page
  ClientRole role;

  /// Creates a call page with given channel name.
  StreamPage(dynamic args) {
    if (args is Map) {
      role = args['role'];
    }
  }

  @override
  _StreamPageState createState() => _StreamPageState();
}

class _StreamPageState extends State<StreamPage> {
  final _users = <int>[];
  final _infoStrings = List<MapEntry<int, String>>();
  Map<int, int> _userIdMapper = {};
  int _userIdCounter = 1;
  bool muted = false;
  int dataStreamId;
  int uid = 0;
  TextEditingController _textEditingController = TextEditingController();
  RtcEngine _engine;

  @override
  void dispose() {
    _users.clear();
    _engine.leaveChannel();
    _engine.destroy();
    super.dispose();
  }

  @override
  void initState() {
    super.initState();
    initialize();
  }

  Future<void> initialize() async {
    if (AppConstants.APP_ID.isEmpty) {
      setState(() {
        _infoStrings.add(MapEntry(
            0, 'APP_ID missing, please provide your APP_ID in settings.dart'));
        _infoStrings.add(MapEntry(0, 'Agora Engine is not starting'));
      });
      return;
    }

    await _initAgoraRtcEngine();
    _addAgoraEventHandlers();
    await _engine.enableWebSdkInteroperability(true);
    VideoEncoderConfiguration configuration = VideoEncoderConfiguration();
    configuration.dimensions = VideoDimensions(1920, 1080);
    await _engine.setVideoEncoderConfiguration(configuration);
    await _engine.joinChannel(
        AppConstants.APP_TOKEN, AppConstants.DEFAULT_CHANNEL, null, 0);
    dataStreamId = await _engine.createDataStream(false, false);
  }

  /// Create agora sdk instance and initialize
  Future<void> _initAgoraRtcEngine() async {
    _engine = await RtcEngine.create(AppConstants.APP_ID);
    await _engine.enableVideo();
    await _engine.setChannelProfile(ChannelProfile.LiveBroadcasting);
    await _engine.setClientRole(widget.role);
  }

  /// Add agora event handlers
  void _addAgoraEventHandlers() {
    _engine.setEventHandler(RtcEngineEventHandler(error: (code) {
      setState(() {
        final info = 'onError: $code';
        _infoStrings.insert(0, MapEntry(0, info));
      });
    }, joinChannelSuccess: (channel, uid, elapsed) {
      setState(() {
        uid = uid;
      });
    }, leaveChannel: (stats) {
      setState(() {
        _users.clear();
      });
    }, userJoined: (uid, elapsed) {
      setState(() {
        final info = 'Joined chat.';
        _infoStrings.insert(0, MapEntry(uid, info));
        _users.add(uid);
        _userIdMapper.putIfAbsent(uid, () => _userIdCounter++);
      });
    }, userOffline: (uid, elapsed) {
      setState(() {
        final info = 'Left chat.';
        _infoStrings.insert(0, MapEntry(uid, info));
        _users.remove(uid);
      });
    }, streamMessage: (uid, streamId, data) {
      setState(() {
        _infoStrings.insert(0, MapEntry(uid, "says: $data"));
      });
    }));
  }

  /// Helper function to get list of native views
  List<Widget> _getRenderViews() {
    final List<StatefulWidget> list = [];
    if (widget.role == ClientRole.Broadcaster) {
      list.add(RtcLocalView.SurfaceView());
    }
    _users.forEach((int uid) => list.add(RtcRemoteView.SurfaceView(uid: uid)));
    return list;
  }

  /// Video view wrapper
  Widget _videoView(view) {
    return Expanded(child: Container(child: view));
  }

  /// Video view row wrapper
  Widget _expandedVideoRow(List<Widget> views) {
    final wrappedViews = views.map<Widget>(_videoView).toList();
    return Expanded(
      child: Row(
        children: wrappedViews,
      ),
    );
  }

  /// Video layout wrapper
  Widget _viewRows() {
    final views = _getRenderViews();
    switch (views.length) {
      case 1:
        return Container(
            child: Column(
          children: <Widget>[_videoView(views[0])],
        ));
      case 2:
        return Container(
            child: Column(
          children: <Widget>[
            _expandedVideoRow([views[0]]),
            _expandedVideoRow([views[1]])
          ],
        ));
      case 3:
        return Container(
            child: Column(
          children: <Widget>[
            _expandedVideoRow(views.sublist(0, 2)),
            _expandedVideoRow(views.sublist(2, 3))
          ],
        ));
      case 4:
        return Container(
            child: Column(
          children: <Widget>[
            _expandedVideoRow(views.sublist(0, 2)),
            _expandedVideoRow(views.sublist(2, 4))
          ],
        ));
      default:
    }
    return Container();
  }

  Widget _bottomBar() {
    return Container(
      alignment: Alignment.bottomCenter,
      padding: const EdgeInsets.symmetric(vertical: 12, horizontal: 8),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Expanded(
              child: Card(
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(30)),
            child: TextFormField(
              controller: _textEditingController,
              style:
                  Theme.of(context).textTheme.subtitle1.copyWith(height: 1.35),
              textInputAction: TextInputAction.send,
              onFieldSubmitted: (val) {
                setState(() {
                  _engine.sendStreamMessage(dataStreamId, val);
                  _infoStrings.insert(0, MapEntry(uid, "says: $val"));
                  _textEditingController.clear();
                });
              },
              decoration: InputDecoration(
                  hintText: "Type to chat...",
                  prefixIcon: Icon(MaterialCommunityIcons.chat_processing,
                      color: Colors.redAccent),
                  border: InputBorder.none),
            ),
          )),
          Container(
            padding: EdgeInsets.all(8),
            child: InkWell(
              child: CircleAvatar(
                child: Icon(Icons.mail, color: Colors.white),
                backgroundColor: Colors.blueGrey[700].withOpacity(0.90),
              ),
              onTap: () {},
            ),
          ),
          Container(
            padding: EdgeInsets.all(8),
            child: InkWell(
              child: CircleAvatar(
                child: Icon(Icons.ios_share, color: Colors.white),
                backgroundColor: Colors.blueGrey[700].withOpacity(0.90),
              ),
              onTap: () {
                showModalBottomSheet(
                    context: context,
                    backgroundColor: Colors.transparent,
                    builder: (context) {
                      return Card(
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.only(
                                topLeft: Radius.circular(32),
                                topRight: Radius.circular(32))),
                        child: Container(
                            padding: EdgeInsets.all(24),
                            child: Column(
                              mainAxisSize: MainAxisSize.min,
                              children: [
                                Row(children: [
                                  Expanded(
                                      child: CircleAvatar(
                                          backgroundColor: Colors.cyan,
                                          radius: 36,
                                          child: Icon(FontAwesome.address_book,
                                              size: 36, color: Colors.white))),
                                  Expanded(
                                      child: CircleAvatar(
                                          backgroundColor: Colors.blue,
                                          radius: 36,
                                          child: Icon(FontAwesome.facebook_f,
                                              size: 36, color: Colors.white))),
                                  Expanded(
                                      child: CircleAvatar(
                                          backgroundColor: Colors.blueAccent,
                                          radius: 36,
                                          child: Icon(
                                              MaterialCommunityIcons
                                                  .facebook_messenger,
                                              size: 36,
                                              color: Colors.white))),
                                ]),
                                SizedBox(height: 40),
                                Row(children: [
                                  Expanded(
                                      child: CircleAvatar(
                                          backgroundColor: Colors.green,
                                          radius: 36,
                                          child: Icon(
                                              MaterialCommunityIcons.chat,
                                              size: 36,
                                              color: Colors.white))),
                                  Expanded(
                                      child: CircleAvatar(
                                          backgroundColor: Colors.redAccent,
                                          radius: 36,
                                          child: Icon(
                                              MaterialCommunityIcons.mail,
                                              size: 36,
                                              color: Colors.white))),
                                  Expanded(
                                      child: CircleAvatar(
                                          backgroundColor: Colors.red,
                                          radius: 36,
                                          child: Icon(
                                              MaterialCommunityIcons.pinterest,
                                              size: 36,
                                              color: Colors.white))),
                                ])
                              ],
                            )),
                      );
                    });
              },
            ),
          ),
          Container(
            padding: EdgeInsets.all(8),
            child: InkWell(
              child: CircleAvatar(
                child: Icon(Icons.wallet_giftcard, color: Colors.white),
                backgroundColor: Colors.orange[700].withOpacity(0.90),
              ),
              onTap: () {},
            ),
          ),
        ],
      ),
    );
  }

  /// Info panel to show logs
  Widget _panel() {
    return Container(
      padding: const EdgeInsets.symmetric(vertical: 48),
      alignment: Alignment.bottomCenter,
      child: FractionallySizedBox(
        heightFactor: 0.5,
        child: Container(
          padding: const EdgeInsets.symmetric(vertical: 48),
          child: ListView.builder(
            reverse: true,
            itemCount: _infoStrings.length,
            itemBuilder: (BuildContext context, int index) {
              if (_infoStrings.isEmpty) {
                return null;
              }
              return Container(
                padding:
                    const EdgeInsets.symmetric(vertical: 3, horizontal: 10),
                child: Row(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    CircleAvatar(
                      radius: 20,
                      backgroundImage: NetworkImage(
                          "https://i.pravatar.cc/150?img=${_userIdMapper[_infoStrings[index].key]}"),
                    ),
                    SizedBox(width: 8),
                    Flexible(
                      child: Container(
                        padding: EdgeInsets.all(8),
                        child: Text(
                          _infoStrings[index].value,
                          style: TextStyle(color: Colors.black),
                        ),
                        decoration: BoxDecoration(
                            color: Colors.white.withOpacity(0.8),
                            borderRadius: BorderRadius.circular(12)),
                      ),
                    )
                  ],
                ),
              );
            },
          ),
        ),
      ),
    );
  }

  void _onToggleMute() {
    setState(() {
      muted = !muted;
    });
    _engine.muteLocalAudioStream(muted);
  }

  void _onSwitchCamera() {
    _engine.switchCamera();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      body: Center(
        child: Stack(
          children: <Widget>[
            _viewRows(),
            _panel(),
            _bottomBar(),
            _topBarWidget(),
          ],
        ),
      ),
    );
  }

  Widget _topBarWidget() {
    return Container(
      alignment: Alignment.topCenter,
      margin: EdgeInsets.symmetric(vertical: 24),
      padding: EdgeInsets.all(8),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            children: [
              Container(
                margin: EdgeInsets.all(4),
                padding: EdgeInsets.all(8),
                child: Row(
                  children: [
                    CircleAvatar(
                      radius: 20,
                      backgroundImage:
                          NetworkImage("https://i.pravatar.cc/150?img=0"),
                    ),
                    SizedBox(width: 8),
                    InkWell(
                      onTap: _onToggleMute,
                      child: CircleAvatar(
                        backgroundColor: Colors.white,
                        child: Icon(muted ? Icons.mic_off : Icons.mic,
                            color: Colors.redAccent),
                      ),
                    ),
                    SizedBox(width: 8),
                    InkWell(
                      onTap: _onSwitchCamera,
                      child: CircleAvatar(
                        backgroundColor: Colors.white,
                        child:
                            Icon(Icons.switch_camera, color: Colors.redAccent),
                      ),
                    ),
                  ],
                ),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(32),
                    color: Colors.white.withOpacity(0.30)),
              ),
              Expanded(
                child: Container(
                    height: 50,
                    padding: EdgeInsets.all(8),
                    child: ListView.builder(
                      scrollDirection: Axis.horizontal,
                      itemCount: _users.length,
                      itemBuilder: (context, i) {
                        return CircleAvatar(
                          radius: 20,
                          backgroundImage: NetworkImage(
                              "https://i.pravatar.cc/150?img=${_userIdMapper[_users[i]]}"),
                        );
                      },
                    ),
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(32),
                        color: Colors.white.withOpacity(0.30))),
              ),
              Container(
                margin: EdgeInsets.all(8),
                child: InkWell(
                  onTap: () => Navigator.of(context).pop(),
                  child: CircleAvatar(
                    radius: 18,
                    backgroundColor: Colors.white,
                    child: Icon(Icons.close_rounded, color: Colors.black87),
                  ),
                ),
              )
            ],
          ),
          SizedBox(height: 4),
          Card(
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(30)),
            elevation: 4,
            child: Container(
              padding: EdgeInsets.symmetric(horizontal: 12, vertical: 8),
              child: Row(mainAxisSize: MainAxisSize.min, children: [
                Icon(
                  FontAwesome.diamond,
                  color: Colors.red,
                  size: 16,
                ),
                SizedBox(width: 8),
                Text("5000",
                    style: Theme.of(context).textTheme.button.copyWith(
                        fontWeight: FontWeight.w800, color: Colors.red)),
              ]),
            ),
          )
        ],
      ),
    );
  }
}
