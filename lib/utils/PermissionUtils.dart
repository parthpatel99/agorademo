import 'package:permission_handler/permission_handler.dart';

class PermissionUtils {
  static Future<void> handlePermission(Permission permission) async {
    final status = await permission.request();
    print(status);
  }
}
