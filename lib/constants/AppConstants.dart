import 'package:flutter/material.dart';

class AppConstants {
  static const String APP_ID = "ab5ed0fb37ad4cf19f579c8a23b7d351";
  static const String APP_TOKEN = "";
  static const String DEFAULT_CHANNEL = "testChannel";
}

class ColorConstants {
  static const Color BACKGROUND = Color(0xFFF5F5F5);
  static const Color PRIMARY = Color(0xFF028FFF);
  static const Color PRIMARY_DARK = Color(0xFF1B54D9);
  static const Color PRIMARY_LIGHT = Color(0xFF3EA6F9);
  static const Color PRIMARY_EXTRA_LIGHT = Color(0xFFE3EBFC);
  static const Color PRIMARY_EXTRA_LIGHT2 = Color(0xFFF9FBFF);
  static const Color ACCENT = Color(0xFF00A3F0);
  static const Color ACCENT_DARK = Color(0xFF00A3F0);
  static const Color ACCENT_LIGHT = Color(0xFF95F9DA);
  static const Color GREY_LIGHT = Color(0xFFCED0D8);
  static const Color GREY_DARK = Color(0xFF484848);

  static const Color BUTTON_DARK = Color(0xFF1C4B9C);
  static const Color BUTTON_LIGHT = Color(0xFF3D9ADE);

}
